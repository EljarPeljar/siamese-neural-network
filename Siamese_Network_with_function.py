# -*- coding: utf-8 -*-
"""
Created on Wed Nov 29 13:57:55 2023

@author: eljar
"""



from tensorflow import keras
from keras.layers import Input, Conv2D, Lambda, Concatenate, Add, Multiply, Dense, Flatten,MaxPooling2D,Activation, Dropout
from keras.models import Model, Sequential
from tensorflow.keras.optimizers.schedules import ExponentialDecay
from keras.regularizers import l2
from keras import backend as K
from keras.optimizers import Adam
from skimage.io import imshow
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from tensorflow.keras.optimizers.schedules import ExponentialDecay
import os
import random
from keras.layers import Conv2D, BatchNormalization, Activation, MaxPooling2D, GlobalAveragePooling2D, Dropout, Dense
from keras.models import Sequential


"TRAINING DATA"

test_sign = pd.read_csv(r"C:\Users\eljar\Documents\Dat390\sign_mnist_test.csv")
train_sign = pd.read_csv(r"C:\Users\eljar\Documents\Dat390\sign_mnist_train.csv")


train_sign = train_sign.dropna()
test_sign = test_sign.dropna()

grouped = train_sign.groupby('label')


One_image_list = grouped.apply(lambda x: x.sample(1)).reset_index(level=0, drop=True)


sampled_indices = One_image_list.index


One_image_list['label'] = 0

One_image_list.loc[One_image_list['label'].index.isin(train_sign[train_sign['label'] == 5].index), 'label'] = 1


train_sign.loc[train_sign['label'] != 5, 'label'] = 0
test_sign.loc[test_sign["label"] != 5, "label"] = 0

train_sign.loc[train_sign['label'] == 5, 'label'] = 1
test_sign.loc[test_sign["label"] == 5, "label"] = 1

"Making an alternative set for One Shot Learning"
#One_image_list = train_sign.sample(n=40, random_state=31)
#sampled_indices = One_image_list.index

train_sign = train_sign.drop(index=sampled_indices)

test_labels = test_sign["label"].values
train_labels = train_sign['label'].values

One_label_list = One_image_list["label"].values
One_image_list = One_image_list.drop("label", axis=1).values
One_image_list = One_image_list.reshape(-1, 28, 28)



"For normal learning"

test_sign = test_sign.drop('label', axis=1).values
test_sign = test_sign.reshape(-1, 28, 28)
train_sign = train_sign.drop('label', axis=1).values
train_sign = train_sign.reshape(-1, 28, 28)


# Assuming train_sign is your training images reshaped to (-1, 28, 28)
# And train_labels is a binary label array where '5' is 1 and others are 0
#image_list = train_sign
#label_list = train_labels

def create_pairs(image_list, label_list, num_pairs):
    left_input, right_input, targets = [], [], []

    for i in range(len(label_list)):
        for _ in range(num_pairs):
            compare_to = i
            while compare_to == i:
                compare_to = random.randint(0, len(label_list) - 1)
            left_input.append(image_list[i])
            right_input.append(image_list[compare_to])
            
            targets.append(int(label_list[i] == label_list[compare_to]))


    left_input = np.array(left_input).reshape(-1, 28, 28, 1)
    right_input = np.array(right_input).reshape(-1, 28, 28, 1)
    targets = np.array(targets)

    return left_input, right_input, targets

# Usage of the function
pairs = 5
val_pairs = 5
test_pairs = 5

# For training data
train_left, train_right, train_targets = create_pairs(One_image_list, One_label_list, pairs)


val_image_list = train_sign
val_label_list = train_labels
# For validation data
val_left, val_right, val_targets = create_pairs(val_image_list, val_label_list, val_pairs)


test_image_list = test_sign
test_label_list = test_labels
# For testing data
test_left, test_right, test_targets = create_pairs(test_image_list, test_label_list, test_pairs)



"MODEL"


left_input_tensor = Input((28, 28, 1))
right_input_tensor = Input((28, 28, 1))


convnet = Sequential([
   # First Convolutional Block
    Conv2D(16, (3, 3), input_shape=(28, 28, 1), padding='same'),
    BatchNormalization(),
    Activation('relu'),
    MaxPooling2D(pool_size=(2, 2)),

    # Second Convolutional Block
    Conv2D(32, (3, 3), padding='same'),
    BatchNormalization(),
    Activation('relu'),
    MaxPooling2D(pool_size=(2, 2)),

    # Global Average Pooling
    GlobalAveragePooling2D(),


    Dense(64, activation='relu'),

    # Output
    Dense(18, activation='relu')
])

encoded_l = convnet(left_input_tensor)
encoded_r = convnet(right_input_tensor)


L1_layer = Lambda(lambda tensors: K.abs(tensors[0] - tensors[1]))


L1_distance = L1_layer([encoded_l, encoded_r])


prediction = Dense(1, activation='sigmoid')(L1_distance)


siamese_net = Model(inputs=[left_input_tensor, right_input_tensor], outputs=prediction)


lr_schedule = ExponentialDecay(
    initial_learning_rate=0.001,
    decay_steps=10000,
    decay_rate=0.9
)
optimizer = Adam(learning_rate=lr_schedule)


siamese_net.compile(loss="binary_crossentropy", optimizer=optimizer, metrics=['accuracy'])


siamese_net.summary()


siamese_net.fit([train_left, train_right], train_targets,
                batch_size=8,
                epochs=30,
                verbose=1,
                validation_data=([val_left, val_right], val_targets))



test_loss, test_accuracy = siamese_net.evaluate([test_left, test_right], test_targets)
print("Test Loss:", test_loss)
print("Test Accuracy:", test_accuracy)